#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>

int get_int(char *int_str) {
    int n = 0;
    while (*int_str) {
        n = n * 10 + (*int_str++ - '0');
    }
    return n;
}
void save_in_file(struct stat stbuf, char *full_path, struct dirent *entry, FILE *file){
    char result[255];
    char str_size[255];
    itoa(stbuf.st_size, str_size, 10);
    strcat(result, full_path);
    strcat(result, " ");
    strcat(result, entry->d_name);
    strcat(result, " size: ");
    strcat(result, str_size);
    strcat(result, "\n");

    for (int i = 0; i < 255; i++) {
        fputc((int) result[i], file);
        if (result[i] == '\n') {
            break;
        }
    }
}

int calc_size(char *name_folder, FILE *file, int mini, int maxi) {
    DIR *folder;
    struct dirent *entry;
    struct stat stbuf;
    char full_path[255];
    int counter = 0;


    folder = opendir(name_folder);
    if (!folder) {
        puts("Unable to open directory");
        return -1;
    }

    while ((entry = readdir(folder))) {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        strcpy(full_path, name_folder);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        stat(full_path, &stbuf);

        if (S_ISDIR(stbuf.st_mode)) {
            calc_size(full_path, file, mini, maxi);
        } else if (S_ISREG(stbuf.st_mode) && mini <= stbuf.st_size && stbuf.st_size <= maxi) {
            printf("%s %s size: %ld\n", full_path, entry->d_name, stbuf.st_size);
            save_in_file(stbuf, full_path, entry, file);
        }
        counter += 1;
    }

    closedir(folder);
    return counter;
}

int main(int arg, char **args) {
    char *name_folder = args[1];
    char *file_name = args[2];
    int mini = get_int(args[3]);
    int maxi = get_int(args[4]);
    FILE *file = fopen(file_name, "w");

    int counter = calc_size(name_folder, file, mini, maxi);
    printf("counter file: %d\n", counter);

    char res[255] = "counter file: ";
    char str_counter[255];

    itoa(counter, str_counter, 10);

    strcat(res, str_counter);
    strcat(res, "\n");

    for (int i = 0; i < 255; i++) {
        fputc((int) res[i], file);
        if (res[i] == '\n') {
            break;
        }
    }


    return 0;
}
