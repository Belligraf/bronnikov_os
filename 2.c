#include <stdio.h>

int main(int arg, char **file_name) {
    int check_write;
    FILE *fp = fopen(file_name[1], "w");
    if (!fp){
        puts("file cant be open");
        return 1;
    }
    int input;
    int second_input;
    while (1) {
        input = getc(stdin);
        if (input == 6){
            second_input = getc(stdin);
            if (second_input == 10){
                break;
            }
            check_write = fputc(input, fp);
            int two_check_write = fputc(second_input, fp);

            if (!check_write || !two_check_write){
                puts("character cant be written to file");
                return 1;
            }
            continue;
        }
        check_write = fputc(input, fp);
        if (!check_write){
            puts("character cant be written to file");
            return 1;
        }
    }
    int check = fclose(fp);

    if (check){
        puts("file cant be close");
        return 1;
    }
    return 0;
}
