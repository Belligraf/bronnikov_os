#include <stdio.h>

int main (int arg, char **args){
    char *first_file_name = args[1];
    char *second_file_name = args[2];
    char ch;

    FILE *first_file = fopen(first_file_name, "r");
    FILE *second_file = fopen(second_file_name, "a");

    if (!first_file){
        puts("first_file cant be open");
        return 1;
    }

    if (!second_file){
        puts("second_file cant be open");
        return 1;
    }

    while ((ch = getc(first_file)) != EOF){
        fputc(ch, second_file);
    }

    int f_check = fclose(first_file);
    int s_check = fclose(second_file);

    if (f_check || s_check){
        puts("first_file or second_file cant be close");
        return 1;
    }

    return 0;
}

