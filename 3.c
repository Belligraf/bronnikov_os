#include <stdio.h>

int get_int(char *int_str){
    int n = 0;
    while (*int_str){
        n = n * 10 + (*int_str++ - '0');
    }
    return n;
}

int main (int arg, char **args){
    char *file_name = args[1];
    int n = 0;
    int ch;
    int line_count = 0;
    FILE *fp = fopen(file_name, "r");

    if (!fp){
        puts("file cant be open");
        return 1;
    }

    if (args[2]){
         n = get_int(args[2]);
    }

    while ((ch = getc(fp)) != EOF){
        printf("%c", ch);
        if (ch == 10){
            line_count++;
        }
        if (n != 0 && line_count == n){
            getc(stdin);
            line_count = 0;
        }
    }

    int check = fclose(fp);
    if (check){
        puts("file cant be close");
        return 1;
    }

    return 0;
}
