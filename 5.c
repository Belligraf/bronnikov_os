#include <stdio.h>
#include <dirent.h>

void print_dir_list(char *name_folder, DIR *folder) {
    struct dirent *entry;

    printf("%s\n", name_folder);
    while (entry = readdir(folder)) {
        printf("%s\n", entry->d_name);
    }
}

int main(int arg, char **args) {
    DIR *this_folder;
    DIR *folder_to;
    char *name_folder = args[1];

    this_folder = opendir(".");
    folder_to = opendir(name_folder);

    if (!this_folder || !folder_to) {
        puts("Unable to read directory");
        return 1;
    }

    print_dir_list("----------this_folder----------", this_folder);
    print_dir_list("----------folder_goto----------", folder_to);

    int check_1 = closedir(this_folder);
    int check_2 = closedir(folder_to);

    if (!(check_1 && check_2)){
        puts("Dirs cant be close");
    }

    return 0;
}

