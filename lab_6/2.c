#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*
 * Написать программу поиска одинаковых по их содержимому файлов в двух каталогов, например, Dir1 и Dir2.
 * Пользователь задаёт имена Dir1 и Dir2.
 * В результате работы программы файлы, имеющиеся в Dir1, сравниваются с файлами в Dir2 по их содержимому.
 * Процедуры сравнения должны запускаться в отдельном процессе для каждой пары сравниваемых файлов.
 * Каждый процесс выводит на экран свой pid, имя файла, общее число просмотренных байт и результаты сравнения.
 * Число одновременно работающих процессов не должно превышать N (вводится пользователем).
 */

int get_int(char *int_str) {
    int n = 0;
    while (*int_str) {
        n = n * 10 + (*int_str++ - '0');
    }
    return n;
}

void comparison(char *file_name_1, char *file_name_2) {
    char ch_1;
    char ch_2;
    char *result = "equal";
    long counter = 0;

    FILE *first_file = fopen(file_name_1, "r");
    FILE *second_file = fopen(file_name_2, "r");

    if (!(first_file && second_file)) {
        puts("Files cant be open");
    }

    while (1) {
        ch_1 = getc(first_file);
        ch_2 = getc(second_file);
        counter += 1;

        if (ch_1 != ch_2) {
            result = "different";
            break;
        }

        if (ch_1 == EOF) {
            break;
        }
    }

    int f_check = fclose(first_file);
    int s_check = fclose(second_file);

    if (f_check || s_check) {
        puts("first_file or second_file cant be close");
        return;
    }

//    printf("%s %s pid=%d %s bytes=%ld\n", file_name_1, file_name_2, getpid(), result, counter);
}

void get_info(char *start_or_end, char *full_path_1, char *full_path_2) {
    int hours, minutes, seconds;
    time_t now;
    time(&now);
    struct tm *local = localtime(&now);
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);
    long mili = start.tv_nsec / 10000;

    hours = local->tm_hour;
    minutes = local->tm_min;
    seconds = local->tm_sec;
    printf("%s compared %s %s time: %d:%d:%d:%ld\n",
           start_or_end, full_path_1, full_path_2, hours, minutes, seconds, mili);
}

int main(int arg, char **args) {
    char *first_folder_name = args[1];
    char *second_folder_name = args[2];

    int max_active = get_int(args[3]);
    int number_active = 0;
    int status;
    DIR *folder_1 = opendir(first_folder_name);
    DIR *folder_2 = opendir(second_folder_name);

    if (!(folder_1 && folder_2)) {
        puts("Dir cant be open");
        return 0;
    }

    pid_t pid;
    pid_t wait(int *status);

    struct dirent *entry_1;
    struct dirent *entry_2;

    while (entry_1 = readdir(folder_1)) {
        if (!strcmp(entry_1->d_name, ".") || !strcmp(entry_1->d_name, "..")) {
            continue;
        }

        char full_path_1[255];

        strcpy(full_path_1, first_folder_name);
        strcat(full_path_1, "/");
        strcat(full_path_1, entry_1->d_name);

        while (entry_2 = readdir(folder_2)) {
            if (!strcmp(entry_2->d_name, ".") || !strcmp(entry_2->d_name, "..")) {
                continue;
            }

            char full_path_2[255];
            strcpy(full_path_2, second_folder_name);
            strcat(full_path_2, "/");
            strcat(full_path_2, entry_2->d_name);

            ++number_active;

            for (; number_active > max_active; --number_active) {
                wait(&status);
                printf("del fork, count: %d - 1\n", number_active-1);
            }

            get_info("start", full_path_1, full_path_2);
            printf("add fork, count: %d + 1\n", number_active-1);
            pid = fork();

            if (pid == 0) {
                comparison(full_path_1, full_path_2);
                get_info("finis", full_path_1, full_path_2 );
                return 0;
            } else {
                continue;
            }

        }
        rewinddir(folder_2);
    }
    for (; number_active != 0; --number_active) {
        wait(&status);
    }

    return 0;
}
